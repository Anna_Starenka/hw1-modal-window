import { useState } from 'react'

import './App.css'
import ModalImage from './components/ModalImage'
import ModalText from './components/ModalText'
import Button from './components/Button/Button'

function App() {

const [isImageModalOpen, setIsImageModalOpen] = useState(false)
const [isTextModalOpen, setIsTextModalOpen] = useState(false)

const openImageModal=()=>setIsImageModalOpen(true)
const openTextModal=()=>setIsTextModalOpen(true)

const closeImageModal=()=>setIsImageModalOpen(false)
const closeTextModal=()=>setIsTextModalOpen(false)

  return (
    <>
    <Button type={'button'} classNames={'button'} onClick={openImageModal} >
      Open first modal
    </Button>
    <Button type={'button'} classNames={'button'} onClick={openTextModal} >
      Open second modal
    </Button>

    {isImageModalOpen &&(
      <ModalImage
      className={'image'}
      title={'Product Delete!'}
      text={'By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.'}
      onClose={closeImageModal}
      />
    )}

    {isTextModalOpen &&(
      <ModalText
      title={'Add Product “NAME”'}
      text={'Description for you product'}
      onClose={closeTextModal}
      />
    )}
    </>
  )
}

export default App
