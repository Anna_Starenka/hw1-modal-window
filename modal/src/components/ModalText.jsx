import ModalBody from "./Modal/ModalBody"
import ModalFooter from "./Modal/ModalFooter"
import ModalHeader from "./Modal/ModalHeader"
import ModalWrapper from './Modal/ModalWrapper'
import ModalClose from './Modal/ModalClose'
import Modal from "./Modal/Modal"




export default  function ModalText({title, text, onClose}){

    return(
    <>
        <ModalWrapper onClose={onClose}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={onClose}/>
                </ModalHeader>
                <ModalBody>
                    <h2>{title}</h2>
                    <p>{text}</p>
                </ModalBody>
                <ModalFooter
                    firstText = "ADD TO FAVORITE"
                    firstClick ={onClose}
                />
            </Modal>
        </ModalWrapper>
    </>
    )
}