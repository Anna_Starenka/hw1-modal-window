import ModalBody from "./Modal/ModalBody"
import ModalFooter from "./Modal/ModalFooter"
import ModalHeader from "./Modal/ModalHeader"
import ModalWrapper from './Modal/ModalWrapper'
import ModalClose from './Modal/ModalClose'
import Modal from "./Modal/Modal"




export default  function ModalImage({className, title, text, onClose}){

    return(
    <>
        <ModalWrapper onClose={onClose}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={onClose}/>
                </ModalHeader>
                <ModalBody>
                    <div className={className}></div>
                    <h2>{title}</h2>
                    <p>{text}</p>
                </ModalBody>
                <ModalFooter
                    firstText = "NO, CANCEL"
                    secondaryText ="YES, DELETE"
                    firstClick ={onClose}
                    secondaryClick ={onClose}
                />
            </Modal>
        </ModalWrapper>
    </>
    )
}